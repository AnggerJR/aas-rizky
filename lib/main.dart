import 'package:flutter/material.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/GeneratedSlashscreenWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget1/GeneratedSlashscreenWidget1.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget2/GeneratedSlashscreenWidget2.dart';
import 'package:flutterapp/time_20cutapp/generateddashbardwidget/GeneratedDashbardWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedayoogunseindesibvworyqs0unsplash1widget/GeneratedAyoogunseindesibVwORYqs0unsplash1Widget.dart';
import 'package:flutterapp/time_20cutapp/generatediconpluscirclewidget/GeneratediconpluscircleWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedlearnhowtocookby1pmwidget/GeneratedLearnhowtocookby1pmWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedpickupthekidsby4pmwidget/GeneratedPickupthekidsby4pmWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedgoandvisitmumby7pmwidget/GeneratedGoandvisitMumby7pmWidget.dart';

void main() {
  runApp(time_20cutApp());
}

class time_20cutApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/GeneratedSlashscreenWidget',
      routes: {
        '/GeneratedSlashscreenWidget': (context) =>
            GeneratedSlashscreenWidget(),
        '/GeneratedSlashscreenWidget1': (context) =>
            GeneratedSlashscreenWidget1(),
        '/GeneratedSlashscreenWidget2': (context) =>
            GeneratedSlashscreenWidget2(),
        '/GeneratedDashbardWidget': (context) => GeneratedDashbardWidget(),
        '/GeneratedAyoogunseindesibVwORYqs0unsplash1Widget': (context) =>
            GeneratedAyoogunseindesibVwORYqs0unsplash1Widget(),
        '/GeneratediconpluscircleWidget': (context) =>
            GeneratediconpluscircleWidget(),
        '/GeneratedLearnhowtocookby1pmWidget': (context) =>
            GeneratedLearnhowtocookby1pmWidget(),
        '/GeneratedPickupthekidsby4pmWidget': (context) =>
            GeneratedPickupthekidsby4pmWidget(),
        '/GeneratedGoandvisitMumby7pmWidget': (context) =>
            GeneratedGoandvisitMumby7pmWidget(),
      },
    );
  }
}
