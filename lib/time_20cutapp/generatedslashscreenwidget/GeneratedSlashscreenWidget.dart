import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedGetstartWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedNotificationWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedRectangle1Widget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedBottomleftcornrcircleWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedUndraw_completed_03xt1Widget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/Generated1559Widget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedTopleftroundWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedGetthingsdonewithTODoWidget.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget/generated/GeneratedTodealwithsituationsquicklyandefficientlyWidget.dart';

/* Frame slash screen
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedSlashscreenWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        child: ClipRRect(
      borderRadius: BorderRadius.zero,
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
              height: 1004.0,
              child: Stack(children: [
                Container(
                    width: constraints.maxWidth,
                    child: Container(
                      width: 430.0,
                      height: 932.0,
                      child: Stack(
                          clipBehavior: Clip.none, fit: StackFit.expand,
                          alignment: Alignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.zero,
                              child: Container(
                                color: Color.fromARGB(255, 233, 244, 242),
                              ),
                            ),
                            Positioned(
                              left: 0.0,
                              top: 0.0,
                              right: 0.0,
                              bottom: 0.0,
                              width: null,
                              height: null,
                              child: LayoutBuilder(builder:
                                  (BuildContext context,
                                      BoxConstraints constraints) {
                                final double width =
                                    constraints.maxWidth * 0.25813953488372093;

                                final double height = constraints.maxHeight *
                                    0.020633873509746763;

                                return Stack(children: [
                                  TransformHelper.translate(
                                      x: constraints.maxWidth *
                                          0.6837209302325581,
                                      y: constraints.maxHeight *
                                          0.019313304721030045,
                                      z: 0,
                                      child: Container(
                                        width: width,
                                        height: height,
                                        child: GeneratedNotificationWidget(),
                                      ))
                                ]);
                              }),
                            ),
                            Positioned(
                              left: 69.0,
                              top: 239.0,
                              right: null,
                              bottom: null,
                              width: 239.0,
                              height: 308.0,
                              child: GeneratedUndraw_completed_03xt1Widget(),
                            ),
                            Positioned(
                              left: 50.0,
                              top: 500.0,
                              right: null,
                              bottom: null,
                              width: 337.0,
                              height: 198.0,
                              child: GeneratedGetthingsdonewithTODoWidget(),
                            ),
                            Positioned(
                              left: 27.0,
                              top: 649.0,
                              right: null,
                              bottom: null,
                              width: 382.0,
                              height: 92.0,
                              child: GeneratedRectangle1Widget(),
                            ),
                            Positioned(
                              left: -175.0,
                              top: 542.0,
                              right: null,
                              bottom: null,
                              width: 569.0,
                              height: 201.0,
                              child:
                                  GeneratedTodealwithsituationsquicklyandefficientlyWidget(),
                            ),
                            Positioned(
                              left: 219.0,
                              top: 751.0,
                              right: null,
                              bottom: null,
                              width: 257.0,
                              height: 253.0,
                              child: GeneratedBottomleftcornrcircleWidget(),
                            ),
                            Positioned(
                              left: -31.0,
                              top: -80.0,
                              right: null,
                              bottom: null,
                              width: 231.0,
                              height: 254.0,
                              child: GeneratedTopleftroundWidget(),
                            ),
                            Positioned(
                              left: 26.0,
                              top: 745.0,
                              right: null,
                              bottom: null,
                              width: 378.0,
                              height: 200.0,
                              child: GeneratedGetstartWidget(),
                            ),
                            Positioned(
                              left: 38.0,
                              top: -72.0,
                              right: null,
                              bottom: null,
                              width: 57.0,
                              height: 205.0,
                              child: Generated1559Widget(),
                            )
                          ]),
                    ))
              ])),
        );
      }),
    ));
  }
}
