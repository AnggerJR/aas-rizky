import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget17 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 16.91240692138672,
      height: 25.84995460510254,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M15.8941 0L9.59537 0.0659478C5.15019 0.745692 3.9648 2.99798 3.00572 7.13731C1.53477 13.4528 -0.340289 21.8685 0.0530423 21.9851C0.68345 22.1779 11.4596 26.8144 16.9124 25.6679L15.8941 0Z')
          ..color = Color.fromARGB(255, 161, 224, 233),
      ]),
    );
  }
}
