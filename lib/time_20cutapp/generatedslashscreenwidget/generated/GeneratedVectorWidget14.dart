import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget14 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 17.845388412475586,
      height: 16.80082893371582,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M8.92269 16.8008C13.8506 16.8008 17.8454 13.0398 17.8454 8.40041C17.8454 3.76099 13.8506 0 8.92269 0C3.99483 0 0 3.76099 0 8.40041C0 13.0398 3.99483 16.8008 8.92269 16.8008Z')
          ..color = Color.fromARGB(255, 243, 163, 166),
      ]),
    );
  }
}
