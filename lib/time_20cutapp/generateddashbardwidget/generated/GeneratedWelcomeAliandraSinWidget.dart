import 'package:flutter/material.dart';

/* Text Welcome Aliandra Sin
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedWelcomeAliandraSinWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Welcome Aliandra Sin''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.right,
      style: TextStyle(
        height: 8.0,
        fontSize: 25.0,
        fontFamily: '.ThonburiUI',
        fontWeight: FontWeight.w300,
        color: Color.fromARGB(255, 251, 243, 243),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
