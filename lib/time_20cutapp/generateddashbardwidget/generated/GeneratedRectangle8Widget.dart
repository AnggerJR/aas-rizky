import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/transform/transform.dart';

/* Rectangle Rectangle 8
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRectangle8Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TransformHelper.rotate(
        a: 0.93,
        b: -0.36,
        c: 0.36,
        d: 0.93,
        child: Container(
          width: 43.0,
          height: 2.9513087272644043,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100.0),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(100.0),
            child: Container(
              color: Color.fromARGB(255, 13, 68, 68),
            ),
          ),
        ));
  }
}
