import 'package:flutter/material.dart';

/* Text Welcome Onboard!
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedWelcomeOnboardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Welcome Onboard!''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.right,
      style: TextStyle(
        height: 6.25,
        fontSize: 32.0,
        fontFamily: '.ThonburiUI',
        fontWeight: FontWeight.w300,
        color: Color.fromARGB(255, 0, 0, 0),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
