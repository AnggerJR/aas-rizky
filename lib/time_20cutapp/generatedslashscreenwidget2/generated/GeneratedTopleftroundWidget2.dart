import 'package:flutter/material.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget2/generated/GeneratedEllipse1Widget2.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget2/generated/GeneratedEllipse2Widget2.dart';
import 'package:flutterapp/time_20cutapp/generatedslashscreenwidget2/generated/Generated1559Widget2.dart';

/* Group top left round
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTopleftroundWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 231.0,
      height: 254.0,
      child: Stack(
          clipBehavior: Clip.none, fit: StackFit.expand,
          alignment: Alignment.center,
          children: [
            Positioned(
              left: 31.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 200.0,
              height: 200.0,
              child: GeneratedEllipse2Widget2(),
            ),
            Positioned(
              left: 0.0,
              top: 54.0,
              right: null,
              bottom: null,
              width: 200.0,
              height: 200.0,
              child: GeneratedEllipse1Widget2(),
            ),
            Positioned(
              left: 69.0,
              top: 8.0,
              right: null,
              bottom: null,
              width: 57.0,
              height: 205.0,
              child: Generated1559Widget2(),
            )
          ]),
    );
  }
}
