import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget65 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 26.53777503967285,
      height: 51.89088821411133,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M6.6719 0L14.3768 1.05431C14.7017 1.09877 15.0066 1.26002 15.249 1.51559C15.4914 1.77115 15.6592 2.1083 15.7291 2.48006L16.9544 9.00161L24.5927 19.4639C24.5927 19.4639 27.3355 25.4456 26.307 32.2249C25.2785 39.0042 24.8264 44.0415 24.8264 44.0415C24.8264 44.0415 4.33678 52.977 3.65107 51.7807C2.96536 50.5843 2.74283 45.1502 2.74283 45.1502L0 9.65874C0 9.65874 3.0857 3.67703 4.79999 3.67703C5.90637 3.63364 5.76443 0.737407 6.6719 0Z')
          ..color = Color.fromARGB(255, 161, 224, 233),
      ]),
    );
  }
}
