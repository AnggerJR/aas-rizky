import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget41 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1.9980502128601074,
      height: 5.001862049102783,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M0.0677539 4.48855C0.574176 3.97811 0.96367 3.32936 1.20125 2.60056C1.43884 1.87176 1.51707 1.08574 1.42894 0.313086C1.38271 -0.0829658 1.91546 -0.113462 1.96139 0.280029C2.05535 1.13422 1.96804 2.00218 1.70708 2.80806C1.44612 3.61395 1.01945 4.33324 0.46436 4.9031C0.415907 4.96191 0.350021 4.99706 0.280088 5.00141C0.210155 5.00575 0.14142 4.97896 0.0878581 4.92648C0.0355824 4.87129 0.00414605 4.79433 0.000381537 4.71233C-0.00338297 4.63033 0.0208286 4.54991 0.0677539 4.48855Z')
          ..color = Color.fromARGB(255, 255, 255, 255),
      ]),
    );
  }
}
