import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget45 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 2.397672414779663,
      height: 12.397879600524902,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M0.0386842 11.9084C1.76372 8.41185 2.29231 4.28752 1.51905 0.357816C1.44216 -0.0314561 1.96559 -0.149726 2.04239 0.239073C2.8405 4.33017 2.28293 8.62086 0.480941 12.2549C0.308981 12.6011 -0.132341 12.2528 0.0386842 11.9084Z')
          ..color = Color.fromARGB(255, 255, 255, 255),
      ]),
    );
  }
}
