import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget88 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 19.894071578979492,
      height: 31.534822463989258,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M5.86254 26.7417C3.7591 28.0769 1.79238 29.6849 0 31.5348C3.0264 27.3724 5.23827 22.4942 6.48223 17.2385C6.49729 17.1754 6.51248 17.1118 6.52714 17.0486L6.56941 16.8697C8.0643 10.5969 10.9648 4.89847 14.9743 0.357238L15.2895 0L15.5118 0.441471C16.7409 2.87232 18.2118 5.12642 19.8941 7.15724C17.3163 15.6174 12.3002 22.6587 5.86254 26.7417Z')
          ..color = Color.fromARGB(255, 0, 0, 0),
      ]),
    );
  }
}
