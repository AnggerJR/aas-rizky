import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget73 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 35.8762092590332,
      height: 26.956026077270508,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M32.2592 26.9366C31.6864 26.9982 31.109 26.913 30.5676 26.687C30.0262 26.461 29.534 26.0996 29.1257 25.6285C28.7174 25.1573 28.4028 24.5877 28.2042 23.9598C28.0056 23.3319 27.9278 22.661 27.9762 21.9943L0 4.55693L6.11905 0L32.5134 17.8032C33.4709 17.9721 34.3419 18.5436 34.9613 19.4095C35.5807 20.2753 35.9055 21.3754 35.8741 22.5012C35.8428 23.627 35.4575 24.7004 34.7913 25.5178C34.125 26.3353 33.2241 26.8401 32.2592 26.9366Z')
          ..color = Color.fromARGB(255, 255, 182, 182),
      ]),
    );
  }
}
