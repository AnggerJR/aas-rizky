import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget76 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 33.786643981933594,
      height: 39.29772186279297,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M16.8933 39.2977C26.2232 39.2977 33.7866 30.5006 33.7866 19.6489C33.7866 8.79709 26.2232 0 16.8933 0C7.5634 0 0 8.79709 0 19.6489C0 30.5006 7.5634 39.2977 16.8933 39.2977Z')
          ..color = Color.fromARGB(255, 255, 101, 132),
      ]),
    );
  }
}
