import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget83 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 0.15000000596046448,
      child: Container(
        width: 3.108687162399292,
        height: 1.5938538312911987,
        child: SvgWidget(painters: [
          SvgPathPainter.fill()
            ..addPath(
                'M3.10869 1.48825C2.26686 1.62418 1.41321 1.6289 0.570306 1.50231C0.349487 1.01809 0.158894 0.516025 0 0C0.938484 0.736693 1.99626 1.2431 3.10869 1.48825Z')
            ..color = Color.fromARGB(255, 0, 0, 0),
        ]),
      ),
    );
  }
}
